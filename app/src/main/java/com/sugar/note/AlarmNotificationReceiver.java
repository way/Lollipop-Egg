package com.sugar.note;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class AlarmNotificationReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if(action.equals(PublicUtils.ACTION_ALERT_CLOSE)){
            int notifyId = intent.getIntExtra("notifyId", 0);
            NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(notifyId);
            Log.w("AlarmNotificationReceiver","ACTION_ALERT_CLOSE notifyId:"+ notifyId +" context:"+ context);
        }
    }

}
