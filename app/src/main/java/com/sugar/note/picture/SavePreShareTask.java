package com.sugar.note.picture;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.sugar.note.PublicUtils;
import com.sugar.note.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import com.sugar.note.NoteView;
import android.widget.Toast;

/**
 * Created by user on 9/23/14.
 */
public class SavePreShareTask extends AsyncTask<Object, Integer, String> {

    private ProgressDialog mProgress;
    private Activity mContext;
    public static final String TAG = "SavePreShareTask";

    public SavePreShareTask(Activity context) {
        mContext = context;
    }

    @Override
    protected void onPreExecute() {
        mProgress = new ProgressDialog(mContext);
        mProgress.setMessage(mContext.getResources().getString(R.string.picture_loading));
        mProgress.setProgressStyle(R.style.ProgressDialogStyle);
        mProgress.setCancelable(false);
        mProgress.setCanceledOnTouchOutside(false);
        mProgress.show();
    }

    @Override
    protected String doInBackground(Object... objects) {

        Bitmap bitmap = (Bitmap)objects[0];
        String filePath = (String)objects[1];
        Boolean isCreateBitmap = (Boolean)objects[2];
        
        File sdDir = null;
        boolean sdCardExist = Environment.getExternalStorageState()
                .equals(Environment.MEDIA_MOUNTED); //�ж�sd���Ƿ����
        if (sdCardExist) {
            sdDir = Environment.getExternalStorageDirectory();// ��ȡ��Ŀ¼
            Log.e(TAG, sdDir.toString());
        }

        if (bitmap == null){
            if (isCreateBitmap) {
                if (mContext instanceof NoteView) {
                    bitmap = PublicUtils.convertViewToBitmapEx(((NoteView)mContext).getEditArea());
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }

        File f = new File(filePath);
        if (f.exists()) {
            return filePath;
        }

        //����ļ������ڣ��򽫷���ͼƬ��ʱ������ָ����Ŀ¼�У��ػ�ʱ��ɾ��
        File dir = new File(PublicUtils.FOLD_SHARE_PIC);
        if (sdDir != null) {
            dir = new File(sdDir + "/" + PublicUtils.FOLD_SHARE_PIC_HOME);
        }
        if (!dir.exists()) {
            dir.mkdirs();
            Log.d(TAG,"dir.mkdirs()");
        }
        // fukang.li date20151207 for MYOSL-513

        String path = PublicUtils.FOLD_SHARE_PIC + "/" +getFileNameFromPath(filePath);
        if (sdDir != null) {
            path = sdDir + "/" + PublicUtils.FOLD_SHARE_PIC_HOME + "/" +getFileNameFromPath(filePath);
        }
	 Log.d("tui", "doInBackground path = " + path);
	 Log.d("tui", "doInBackground filePath = " + filePath);

        try {
            FileOutputStream fos = new FileOutputStream(path);
            if (null != fos && bitmap != null) {
                //bitmap.compress(Bitmap.CompressFormat.PNG, 10, fos);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                fos.flush();
                fos.close();
                return filePath;
            }
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String path) {
        Log.d("tui", "onPostExecute path = " + path);
        if (path == null) {
	     Toast.makeText(mContext, R.string.note_share_error, Toast.LENGTH_SHORT).show();
	     if (mProgress != null) {
                mProgress.dismiss();
            }
            return;
        }

        Intent intent = new Intent(Intent.ACTION_SEND);

        File f = new File(path);
        if (f.exists() && f.isFile()) {
            intent.setType("image/*");
            Uri u = Uri.fromFile(f);
            Log.d("tui", "u = " + u);
            intent.putExtra(Intent.EXTRA_STREAM, u);
        }

        //intent.putExtra(Intent.EXTRA_SUBJECT, "Share***********");
        //intent.putExtra(Intent.EXTRA_TEXT, "TEXT-----------------");FLAG_ACTIVITY_SINGLE_TOP
        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
	 final CharSequence chooseTitle = mContext.getText(R.string.note_share_via);
        //mContext.startActivity(Intent.createChooser(intent, mContext.getTitle()));
        mContext.startActivity(Intent.createChooser(intent, chooseTitle));

        if (mProgress != null) {
            mProgress.dismiss();
        }
    }

    private String getFileNameFromPath(String path) {
        String[] p = path.split("/");
        String name = p[p.length - 1];
        Log.d("tui", "getFileNameFromPath mame = " + name);

        //String[] p1 = name.split("\\.");

        return name;
    }
}
